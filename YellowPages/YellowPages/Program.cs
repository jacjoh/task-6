﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using YellowPages;

namespace ConsoleApp4
{
    class YellowPages
    {
        static void Main(string[] args)
        {
            //Initializes a list of persons for the yellow pages
            List<Person> contacts = new List<Person>() { };
            InitializePersons(contacts);

            RunProgram(contacts);
        }

        private static void InitializePersons(List<Person> contacts)
        {
            contacts.Add(new Person("Jon", "Andreasen", 90000000));
            contacts.Add(new Person("Arne", "Arnesen", 90000001));
            contacts.Add(new Person("Lise", "Lotte", 90000002));
            contacts.Add(new Person("Thea", "Wolf", 90000003));
            contacts.Add(new Person());
        }


        private static void RunProgram(List<Person> contacts)
        {
            Console.WriteLine("- This is a console based yellow pages app");
            Console.WriteLine("- To search through the contacts type \"s\" and then the contact to search for");
            Console.WriteLine("- To exit the program type e");

            string choice = "";
            while (choice != "e")
            {
                string userResponse = Console.ReadLine();

                //the first part of the string is the command, and the rest is the name 
                choice = userResponse.Split(" ")[0];
                int indexOfName = userResponse.IndexOf(" ") + 1;
                string name = userResponse.Substring(indexOfName);

                switch (choice)
                {
                    case "s":
                        if(userResponse.Length >= 2) 
                        { 
                            SearchContacts(contacts, name); 
                        }
                        else 
                        {
                            Console.WriteLine("- To search through the contacts type \"s\" and then the contact to search for");
                            Console.WriteLine("- To exit the program type e");
                        }
                        break;
                    case "e":
                        break;
                    default:
                        Console.WriteLine("- To search through the contacts type \"s\" and then the contact to search for");
                        Console.WriteLine("- To exit the program type e");
                        break;
                }
            }
        }

        private static void SearchContacts(List<Person> contacts, string searchString)
        {
            //Gets the fullname of the person and check if it matches the searchstring
            var Matches = contacts.Where(contact => contact.GetFullName(contact).ToLower()
                .Contains(searchString.ToLower()));

            if (!Matches.Any())
            {
                Console.WriteLine("- There were no results for your search.");
                return;
            }
            Console.WriteLine("- Matches: ");
            foreach (Person contact in Matches)
            {
                Console.WriteLine("- " + contact.GetFullName(contact));
            }
        }
    }
}
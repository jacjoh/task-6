﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YellowPages
{
    class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int PhoneNumber { get; set; }

        public Person()
        {
            FirstName = "Gerild";
            LastName = "Braaten";
            PhoneNumber = 91000000;
        }

        public Person(string firstName, string lastName, int phoneNumber)
        {
            FirstName = firstName;
            LastName = lastName;
            PhoneNumber = phoneNumber;
        }

        public string GetFullName(Person person)
        {
            string fullname = person.FirstName + " " + person.LastName;
            return fullname;
        }
    }
}
